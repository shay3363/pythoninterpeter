#ifndef INTEGER_H
#define INTEGER_H
#include "type.h"
class Integer : public Type
{
private:
	int _val;
public:
	Integer(int val);
	virtual ~Integer();
	bool isPrintable() const;
	std::string toString() const;
};
#endif // INTEGER_H