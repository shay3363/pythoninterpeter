#ifndef TYPE_H
#define TYPE_H
#include <string>
class Type
{
private:
	bool _isTemp;
public:
	Type();
	virtual ~Type();
	bool getIsTemp() const;
	void setIsTemp(bool isTemp);
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
};





#endif //TYPE_H
