#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
	this->_name = name;
}

const char * NameErrorException::what() const throw()
{
	return "NameError : name '",this->_name.c_str(),"' is not defined";
}
