#include "parser.h"
#include "InterperterException.h"
#include "type.h"
#include "Void.h"
#include "Helper.h"
#include "Boolean.h"
#include "Integer.h"
#include "String.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <unordered_map>
std::unordered_map<std::string, Type*> Parser::_variables;
Type* Parser::parseString(std::string str) throw()
{
	Type* newVar = NULL;
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '	')
		{
			throw IndentationException();
		}
		if ((newVar = getVariableValue(str)) != NULL)
		{}
		else if ((newVar = getType(str)) == NULL)
		{
			if (makeAssignment(str))
			{
				newVar = new Void();
				newVar->setIsTemp(true);
			}
		}
	}

	return newVar;
}
Type* Parser::getType(std::string &str)
{
	Type* newVar = NULL;
	Helper::trim(str);
	if (Helper::isInteger(str))
	{
		newVar = new Integer(std::stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		newVar = new Boolean(str == "True");
	}
	else if (Helper::isString(str))
	{
		std::string newStr = std::string(str);
		newStr[0] = '\'';
		newStr[newStr.size() - 1] = '\'';
		newVar = new String(newStr);
	}
	if (newVar != NULL)
	{
		newVar->setIsTemp(true);
	}
	return newVar;
}
bool Parser::isLegalVarName(const std::string& str)
{
	bool flag = true;
	int i = 0;
	if (Helper::isDigit(str[0]))
	{
		flag = false;
	}
	for (int i = 1; i < str.size() && flag; i++)
	{
		if (!Helper::isDigit(str[i]) && !Helper::isLetter(str[i]) && !Helper::isUnderscore(str[i]))
		{
			flag = false;
		}
	}
	return flag;
}

bool Parser::makeAssignment(const std::string & str)
{
	bool flag = false;
	if (std::count(str.begin(), str.end(), '=') != 1)
	{
		Type* newVarType = NULL;
		std::string name = std::string(str.begin(),str.begin()+str.find('='));//take all the str before the =
		std::string value = std::string(str.begin()+str.find('=')+1,str.end());//take all the str after the = 
		Helper::trim(value);
		Helper::trim(name);
		if (!isLegalVarName(name))
		{
			throw SyntaxException();
		}
		newVarType = getType(value);
		if (newVarType == NULL)
		{
			throw SyntaxException();
		}
		_variables.insert(std::make_pair(name,newVarType));
		flag = true;
	}
	return flag;
}
Type* Parser::getVariableValue(const std::string &str)
{
	std::unordered_map<std::string,Type*>::iterator it;
	Type* retVal = NULL;
	if ((it = _variables.find(str)) != _variables.end())
	{
		retVal = it->second;
	}
	return retVal;
}