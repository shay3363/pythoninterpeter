#include "Boolean.h"

Boolean::Boolean(bool val) : Type::Type()
{
	this->_val = val;
}

Boolean::~Boolean()
{
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	std::string retString = std::string();
	if (this->_val)
	{
		retString = "True";
	}
	else
	{
		retString = "False";
	}
	return retString;
}
