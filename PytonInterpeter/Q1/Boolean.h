#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"
class Boolean : public Type
{
private:
	bool _val;
public:
	Boolean(bool val);
	virtual ~Boolean();
	bool isPrintable() const;
	std::string toString() const;
};
#endif // BOOLEAN_H