#include "Integer.h"

Integer::Integer(int val) : Type::Type()
{
	this->_val = val;
}

Integer::~Integer()
{
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(this->_val);
}
