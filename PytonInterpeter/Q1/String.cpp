#include "String.h"

String::String(std::string val) : Sequence::Sequence()
{
	this->_val = val;
}

String::~String()
{
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	return this->_val;
}
