#include "type.h"
#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Shay Cohen"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			Type* var = Parser::parseString(input_string);
			if(var!=NULL)
			{
				if (var->isPrintable())
				{
					std::cout << var->toString() << std::endl;
				}
				if (var->getIsTemp())
				{
					delete var;
				}
			}
		}
		catch(IndentationException& ex)
		{ 
			std::cout << ex.what() << std::endl;
		}
		catch (SyntaxException& ex)
		{
			std::cout << ex.what() << std::endl;
		}
		catch (NameErrorException& ex)
		{
			std::cout << ex.what() << std::endl;
		}
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


