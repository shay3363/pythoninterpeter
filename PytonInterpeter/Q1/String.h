#ifndef STRING_H
#define STRING_H
#include "Sequence.h"
class String : public Sequence
{
private:
	std::string _val;
public:
	String(std::string val);
	virtual ~String();
	bool isPrintable() const;
	std::string toString() const;
};
#endif // STRING_H