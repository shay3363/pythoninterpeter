#ifndef VOID_H
#define VOID_H
#include "type.h"
class Void : public Type
{
private:
public:
	Void();
	virtual ~Void();
	bool isPrintable() const;
	std::string toString() const;
};

#endif // VOID_H